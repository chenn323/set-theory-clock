package com.paic.arch.interviews;


public class ClockColor {

	/**
	 * 黄色灯
	 */
	public static String YELLOW = "Y";
	
	/**
	 * 红色灯
	 */
	public static String RED = "R";
	
	/**
	 * 暗灯
	 */
	public static String CLOSE = "O";

}
