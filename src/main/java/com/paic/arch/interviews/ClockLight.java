package com.paic.arch.interviews;

import org.apache.commons.lang.StringUtils;

/**
 * 时钟亮灯状态
 * 
 * @author Chapter
 *
 */
public class ClockLight {

	private String secondLight;
	private String fiveHourLights;
	private String oneHourLights;
	private String fiveMinuteLights;
	private String oneMinuteLights;

	public ClockLight(ClockEntity clock) {
		this.secondLight = setSecondLight(clock);
		this.fiveHourLights = setFiveHourLights(clock);
		this.oneHourLights = setOneHourLights(clock);
		this.fiveMinuteLights = setFiveMinuteLights(clock);
		this.oneMinuteLights = setOneMinuteLights(clock);
	}

	/**
	 * 顶部灯 规则: 两秒亮一次 黄色
	 * 
	 * @param clockTime
	 * @return
	 */
	private String setSecondLight(ClockEntity clockTime) {
		String color = clockTime.getSeconds() % 2 == 0 ? ClockColor.YELLOW : ClockColor.CLOSE;

		return color;
	}

	/**
	 * 第二排的4盏灯 规则: 每5个小时亮灯 红色
	 * 
	 * @param clockTime
	 * @return
	 */
	private String setFiveHourLights(ClockEntity clockTime) {
		int hours = clockTime.getHours();
		int lightNum = hours / 5; // 亮灯的数量
		String[] lights = lightStatus(ClockColor.RED, lightNum, 4);
		return array2str(lights);
	}

	/**
	 * 第三排的4盏灯 规则: 每小时亮灯 红色
	 * 
	 * @param clockTime
	 * @return
	 */
	private String setOneHourLights(ClockEntity clockTime) {
		int hours = clockTime.getHours();
		int lightNum = hours % 5; // 亮灯的数量
		String[] lights = lightStatus(ClockColor.RED, lightNum, 4);
		return array2str(lights);
	}

	/**
	 * 第四排的11盏灯 规则: 每5分钟亮灯 第3、6、9盏灯是红色, 其余为黄色
	 * 
	 * @param clockTime
	 * @return
	 */
	private String setFiveMinuteLights(ClockEntity clockTime) {
		int minutes = clockTime.getMinutes();
		int lightNum = minutes / 5;
		String[] lights = lightStatus(ClockColor.YELLOW, lightNum, 11); // 先将亮灯都设为黄灯
		changeLightColor(lights, lightNum); // 替换相应的灯为红灯
		return array2str(lights);
	}

	/**
	 * 第五排的4盏灯 规则: 每分钟亮灯 黄色
	 * 
	 * @param clockTime
	 * @return
	 */
	private String setOneMinuteLights(ClockEntity clockTime) {
		int minutes = clockTime.getMinutes();
		int lightNum = minutes % 5; // 亮灯的数量
		String[] lights = lightStatus(ClockColor.YELLOW, lightNum, 4);
		return array2str(lights);
	}

	private void changeLightColor(String[] lights, int lightNum) {
		if (lightNum >= 9) {
			lights[2] = ClockColor.RED;
			lights[5] = ClockColor.RED;
			lights[8] = ClockColor.RED;
		}
		else if (lightNum >= 6 && lightNum < 9) {
			lights[2] = ClockColor.RED;
			lights[5] = ClockColor.RED;
		}
		else if (lightNum >= 3 && lightNum < 6) {
			lights[2] = ClockColor.RED;
		}

	}

	/**
	 * 设置灯的颜色状态(红,黄,关闭)
	 * 
	 * @param color
	 *            设为color色
	 * @param lightNum
	 *            灯亮的数量
	 * @param lightTotal
	 *            这一排灯的总数
	 * @return
	 */
	public String[] lightStatus(String color, int lightNum, int lightTotal) {
		String[] lights = new String[lightTotal];
		for (int i = 0; i < lightTotal; i++) {
			lights[i] = i < lightNum ? color : ClockColor.CLOSE;
		}
		return lights;
	}
	
	public String array2str(String[] array){
		return StringUtils.join(array);
	}

	@Override
	public String toString() {
		return StringUtils.join(
				new String[] {secondLight, fiveHourLights, oneHourLights, fiveMinuteLights, oneMinuteLights }, System.lineSeparator());
	}

}
