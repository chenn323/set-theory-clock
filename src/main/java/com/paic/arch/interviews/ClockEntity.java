package com.paic.arch.interviews;

/**
 * 时钟实体类
 * 
 * @author Chapter
 *
 */
public class ClockEntity {

	private int seconds; 	// 秒
	private int minutes; 	// 分
	private int hours; 		// 时

	public ClockEntity(String time) {
		if(time.isEmpty()){
			throw new IllegalArgumentException("testTime is should not be null");
		}
		String[] testTime = time.split(":");
		if (testTime.length != 3) {
			throw new IllegalArgumentException("testTime format error");
		}
		this.seconds = Integer.valueOf(testTime[2]);
		this.minutes = Integer.valueOf(testTime[1]);
		this.hours = Integer.valueOf(testTime[0]);
	}

	public int getSeconds() {
		return seconds;
	}

	public void setSeconds(int seconds) {
		this.seconds = seconds;
	}

	public int getMinutes() {
		return minutes;
	}

	public void setMinutes(int minutes) {
		this.minutes = minutes;
	}

	public int getHours() {
		return hours;
	}

	public void setHours(int hours) {
		this.hours = hours;
	}

}
