package com.paic.arch.interviews;

public class TimeConverterImpl implements TimeConverter {

	@Override
	public String convertTime(String aTime) {
		ClockEntity clock = new ClockEntity(aTime);
		ClockLight clockLight = new ClockLight(clock);
		return clockLight.toString();
	}

}
