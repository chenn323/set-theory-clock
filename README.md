## 欢迎参加平安普惠面试-时钟

Set Theory Clock是一个通过亮灯数量来表示时间的时钟，如图所示：

![HERE](images/settheoryclock.gif)

在钟的顶端有一盏每两秒钟闪烁一次的黄灯。
前两排的灯为红色，每排4盏，共8盏，表示小时，其中第一排每盏灯代表5小时；第二排每盏灯代表1小时。（例如：第一排的两个灯和第二排的三个灯点亮，对应时间为5+5+3=13h或1 pm）
底部两排的灯表示分钟，第一排有11盏灯，每盏灯代表5分钟，其中第3、第6和第9盏的灯是红色，表示15分、30分和45分，其他的灯为黄色；最后一排有4盏灯，每盏灯代表1分钟。

## 简述

您需要让Set Theory Clock的测试用例通过。另外，请您在代码上标注相关的注释，以便我们理解您的思路。

## 提示

如果您没有使用过Gradle，您可能需要花点时间阅读一篇Gradle相关的摘要。另外，案例中我们使用了Gradle Wrapper，因此从命令行中的gradlew会下载您所需的所有内容。

## 评分标准
* 所有测试用例需无错执行。
* 需正确理解题目要求。
* 良好的编码习惯及面向对象的设计思想。
* 请在截止日期前提交您的答案。



## 其他
请填写文件 [FEEDBACK.md](FEEDBACK.md)
=======
**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

---

## Edit a file

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. Click **Source** on the left side.
2. Click the README.md link from the list of files.
3. Click the **Edit** button.
4. Delete the following text: *Delete this line to make a change to the README from Bitbucket.*
5. After making your change, click **Commit** and then **Commit** again in the dialog. The commit page will open and you’ll see the change you just made.
6. Go back to the **Source** page.

---

## Create a file

Next, you’ll add a new file to this repository.

1. Click the **New file** button at the top of the **Source** page.
2. Give the file a filename of **contributors.txt**.
3. Enter your name in the empty file space.
4. Click **Commit** and then **Commit** again in the dialog.
5. Go back to the **Source** page.

Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).
